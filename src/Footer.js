import React from 'react';

class Footer extends React.Component{
  render(){
    return(
      <footer className="Footer">
        <p>&copy; Developed by LAW</p>
      </footer>
    );
  }
}

export default Footer;
