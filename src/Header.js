import React from 'react'
import './App.css'

class Header extends React.Component {
    render() {
        return(
        <div className="Header">
            <p id="p-h1">{this.props.head}</p>
        </div>
        );
    }
}

export default Header;
