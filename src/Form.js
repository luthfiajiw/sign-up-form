import React from 'react';
import './App.css';
import './Validation.js';

class Form extends React.Component{
  render(){
    return(
      <div className="Form">
        <form>

          <label>
            Name:
          </label>
            <br/>
            <input type="text" name="username" placeholder="username"/>
            <p>*Username must be and contain 5 - 12 characters</p>

          <label>
            Email:
          </label>
            <br/>
            <input type="email" name="email" placeholder="email"/>
            <p>*Email must be a valid address, e.g me@mydomain.com</p>

          <label>
            Telephone:
          </label>
            <br/>
            <input type="telp" name="telephone" placeholder="telephone"/>
            <p>*Telephone must be a valid Indonesia telephone number (11/12 digits)</p>


          <label>
            Password:
          </label>
            <br/>
            <input type="password" name="password" placeholder="password"/>
            <p>*Password must be alphanumeric (@, _ and - also allowed and be 8- 20 characters)</p>

          <label>
            Confirm Password:
          </label>
            <br/>
            <input type="password" name="password" placeholder="password"/>
            <p>*Confirm your new password</p>


          <br/>
          <a href="" className="submit">Submit</a>
        </form>
      </div>
    );
  }
}

export default Form;
