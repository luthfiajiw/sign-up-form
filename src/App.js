import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './Header';
import Form from './Form';
import Footer from './Footer';


class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Selamat Datang di React</h1>
        </header>
        <Header head="New User Signup"/>
        <Form/>
        <br/>
        <hr/>
        <Footer/>
        <br/>
      </div>

    );
  }
}

export default App;
